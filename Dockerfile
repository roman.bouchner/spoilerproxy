FROM gradle:jdk8 as builder
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build

FROM openjdk:8-alpine
EXPOSE 8080
COPY --from=builder /home/gradle/src/build/libs/spoiler-proxy.war /app/spoiler-proxy.war
WORKDIR /app
CMD ["java", "-jar", "spoiler-proxy.war"]

# docker build -t spoilerproxy/jdk8:v1.0.12-beta .
# docker tag spoilerproxy/jdk8:v1.0.12-beta spoilerproxy/jdk8:latest

# docker login -u spoilerproxy
# docker push spoilerproxy/jdk8:latest

# docker run --rm -p 1080:1080 -p 8484:8484 spoilerproxy/jdk8:v1.0.13-beta