# Spoilerproxy 
This is an open source implementation of [spoiler proxy](https://www.spoilerproxy.com).

More information available at project page:
https://www.spoilerproxy.com

Screenshots:
https://spoilerproxy.com/screenshots/

## How to build it
```
gradle build
```


## How to run it
```
java -jar build/libs/spoiler-proxy.war
```

## How to use it
Check main application page:
http://localhost:8484
