<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Spoiler Proxy</title>
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
<img src="/img/logo.png"><br>
<div class="version">Version: ${version}</div> <p>
<a href="swagger-ui.html">API documentation</a> for automated tests.<br>
<a href="https://www.spoilerproxy.com">Documentation.</a>

<p>
</p>
<h2>Actual state</h2>
${socksServer.running ? "SOCKS proxy server is running.": "SOCKS server is stopped!"} Port: ${socksServer.port}
<p>
</p>
<h2>Endpoints</h2>
${endpoints.size()==0 ? "No endpoints catched yet. Use proxychains to pass network via Spoiler Proxy.":""}
<br>
<button onclick="location.reload();">Refresh</button>
<button class="clearAll" onclick="clearEndpoints();">Clear all</button>
<p></p>
<form autocomplete="off">
<table class="blueTable">
    <tr><td>ID</td></td><td>Address</td><td>Status</td><td>Conns</td></tr>
<c:forEach var="endpoint" items="${endpoints}">
    <tr>
        <td ${endpoint.connections==0?"class=\"noActive\"":""}>${endpoint.id}</td>
        <td ${endpoint.connections==0?"class=\"noActive\"":""}><c:out value="${endpoint.name}" /></td>
        <td>
            <select class="${endpoint.actualStatus}ColorStatus" onchange="changeStateWithSelect('${endpoint.id}',this)">
                <c:forEach var="status" items="${endpoint.statuses}">
                  <option value="${status.value}" ${status.value.equals(endpoint.actualStatus)?" selected=\"selected\" ":""}>${status.display}</option>
                </c:forEach>
            </select>
        </td>
        <td><c:out value="${endpoint.connections}" /></td>
    </tr>
</c:forEach>
</table>
</form>
.
<script src="/js/main.js"></script>
</body>
</html>