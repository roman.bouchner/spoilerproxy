/**
 * Calls our API to change state
 * @param id
 * @param state
 */
function changeState(id,state) {
    var xhr = new XMLHttpRequest();
    var url = "/api/change-status-by-id";
    var request = {"connectionId":id,"state":state};

    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            //reload page after changing state
            location.reload();
        }
    };
    var data = JSON.stringify(request);
    xhr.send(data);
}

/**
 * Calls our api to change state
 * @param id
 * @param select DOM select element
 */
function changeStateWithSelect(id,select) {
    var state = (select.value || select.options[select.selectedIndex].value);
    changeState(id,state);
}

function clearEndpoints() {
    var xhr = new XMLHttpRequest();
    var url = "/api/clear-endpoints";
    var request = {};

    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            //reload page after changing state
            location.reload();
        }
    };
    var data = JSON.stringify(request);
    xhr.send(data);
}