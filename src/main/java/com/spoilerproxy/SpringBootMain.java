package com.spoilerproxy;

import com.spoilerproxy.endpoint.EndpointService;
import com.spoilerproxy.socks.SocksServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.logging.Logger;

@SpringBootApplication
public class SpringBootMain extends SpringBootServletInitializer {

	Logger logger = Logger.getLogger(SpringBootMain.class.getName());

	@Autowired
	SocksServer socksServer;

	@Autowired
	EndpointService endpointService;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMain.class, args);
	}

	@PostConstruct
	public void start() {
		logger.info("Starting socks server");
		socksServer.start();
	}

	@PreDestroy
	public void stop() {
		logger.info("Stopping socks server");
		socksServer.stop();
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SpringBootMain.class);
	}
}
