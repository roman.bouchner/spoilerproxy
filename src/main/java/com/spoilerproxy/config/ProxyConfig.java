package com.spoilerproxy.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="proxy")
public class ProxyConfig {
	int socksPort;
	int maxConnections;
	String storageFile;

	public int getSocksPort() {
		return socksPort;
	}

	public void setSocksPort(int socksPort) {
		this.socksPort = socksPort;
	}

	public int getMaxConnections() {
		return maxConnections;
	}

	public void setMaxConnections(int maxConnections) {
		this.maxConnections = maxConnections;
	}

	public String getStorageFile() {
		return storageFile;
	}

	public void setStorageFile(String storageFile) {
		this.storageFile = storageFile;
	}
}
