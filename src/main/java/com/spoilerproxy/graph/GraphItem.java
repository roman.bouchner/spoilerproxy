package com.spoilerproxy.graph;

import java.util.Map;

public class GraphItem {
	private final int time;
	private final int connections;
	private final Map<String,Integer> grouped;

	public GraphItem(int time, int connections, Map<String,Integer> grouped) {
		this.time = time;
		this.connections = connections;
		this.grouped = grouped;
	}

	public int getTime() {
		return time;
	}

	public int getConnections() {
		return connections;
	}

	public Map<String, Integer> getGrouped() {
		return grouped;
	}
}
