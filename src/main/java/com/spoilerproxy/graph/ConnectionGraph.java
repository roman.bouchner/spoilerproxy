package com.spoilerproxy.graph;

import com.spoilerproxy.endpoint.EndpointService;
import com.spoilerproxy.socks.SocksServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service
public class ConnectionGraph {
	private LinkedList<Map<String,Integer>> graph = new LinkedList<>();
	private volatile boolean running = true;
	private final int graphLength = 60;

	@Autowired
	EndpointService endpointService;

	@Autowired
	SocksServer socksServer;

	@PostConstruct
	public void start() {
		//start thread
		Thread t = new Thread(runnable);
		t.setDaemon(true);
		t.setName("GraphThread");
		t.start();
	}

	@PreDestroy
	public void stop() {
		running = false;
	}

	public void clear() {
		synchronized(this) {
			//empty all values
			graph.clear();
			for(int i=0; i<graphLength; i++) {
				graph.add(socksServer.getEmptyGrouped());
			}
		}
	}

	private Runnable runnable = ()->{
		clear();
		while (running) {
			Map<String,Integer> grouped = socksServer.getActiveAllConnectionsNumGrouped();
			synchronized (this) {
				graph.add(grouped);
				graph.poll();
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				return;
			}
		}
	};

	public List<GraphItem> getItems() {
		LinkedList<GraphItem> result = new LinkedList<>();
		synchronized (this) {
			Iterator<Map<String, Integer>> it = graph.iterator();
			int i = 0;
			while (it.hasNext()) {
				Map<String, Integer> grouped = it.next();
				int total = 0;
				for (Integer val : grouped.values()) {
					total = total + val;
				}
				result.addFirst(new GraphItem(i++, total, grouped));
			}
		}
		return result;
	}

}
