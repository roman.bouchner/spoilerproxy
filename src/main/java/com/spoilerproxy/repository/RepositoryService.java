package com.spoilerproxy.repository;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.spoilerproxy.config.ProxyConfig;
import com.spoilerproxy.dns.DnsStorageService;
import com.spoilerproxy.dns.Record;
import com.spoilerproxy.endpoint.EndpointInfo;
import com.spoilerproxy.endpoint.EndpointService;
import com.spoilerproxy.socks.ConnectionId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class provides trivial persistent storage to local JSON file.
 */
@Service
public class RepositoryService {

	private Logger logger = Logger.getLogger(RepositoryService.class.getName());

	@Autowired
	EndpointService endpointService;

	@Autowired
	DnsStorageService dnsStorageService;

	@Autowired
	ProxyConfig config;

	private File storageFile;

	private volatile boolean running = true;

	private Object sync = new Object();

	@PostConstruct
	protected void start() {
		if(config.getStorageFile() != null) {
			storageFile = new File(config.getStorageFile());
			//first load from disk
			loadFromDisk();

			//now create thread for receiving saving request
			//the advantage is we do not need to solve synchronization issues
			Thread saveThread = new Thread(() -> {
				while (running) {
					//wait to save request
					try {
						synchronized (sync) {
							sync.wait();
						}
					} catch (InterruptedException e) {
						//exit loop
						return;
					}
					saveToDisk();
				}
			});
			saveThread.setName("RepositoryThread");
			saveThread.start();
		}
	}

	@PreDestroy
	protected void stop() {
		//send stop request to save thread
		synchronized (sync) {
			running = false;
			sync.notify();
		}
	}

	/**
	 * Creates saving request. Writing to disk is done in separated thread to better solve synchronization issues.
	 */
	public void createSaveRequest() {
		synchronized (sync) {
			sync.notify();
		}
	}

	private void saveToDisk() {
		//prepare json
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode root = mapper.createObjectNode();

		//endpoints
		ArrayNode arrayNode = root.putArray("endpoints");
		Iterator<EndpointInfo> it = endpointService.getEndpoins().iterator();
		while (it.hasNext()) {
			ObjectNode endpoinNode = mapper.createObjectNode();
			endpoinNode.put("id",it.next().getId());
			arrayNode.add(endpoinNode);
		}

		//dns
		ArrayNode arrayNodeDns = root.putArray("dns");
		List<Record> recordList = dnsStorageService.getAllRecords();
		for (Record record:recordList) {
			ObjectNode recordNode = mapper.createObjectNode();
			recordNode.put("ip",record.getIp());
			recordNode.put("name",record.getName());
			arrayNodeDns.add(recordNode);
		}


		//save to disk
		try {
			mapper.writerWithDefaultPrettyPrinter().writeValue(storageFile, root);
			logger.info("Endpoinds stored to " + storageFile.getAbsolutePath());
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Cannot store endpoins to " + storageFile.getAbsolutePath() + " " + e.getMessage());
		}

	}

	private void loadFromDisk() {
		if(storageFile.exists()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				ObjectNode root = (ObjectNode)mapper.readTree(storageFile);
				//endpoints
				JsonNode endpoinsArray = root.get("endpoints");
				for(int i=0; i<endpoinsArray.size();i++) {
					String id = endpoinsArray.get(i).get("id").asText();
					endpointService.addConnection(ConnectionId.fromId(id),false);
				}
				//dns
				JsonNode dnsArray = root.get("dns");
				for(int i=0; i<dnsArray.size(); i++) {
					String name = dnsArray.get(i).get("name").asText();
					dnsStorageService.addDomain(name,false);
				}
				logger.info("Endpoinds loaded from " + storageFile.getAbsolutePath());
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Cannot load endpoins from " + storageFile.getAbsolutePath() + " " + e.getMessage());
			}
		}
	}
}
