package com.spoilerproxy.api.response;

import io.swagger.annotations.ApiModelProperty;

public class EndpointStatusResponse {
	@ApiModelProperty(notes = "Value used in API", position = 1, required = true)
	String value;
	@ApiModelProperty(notes = "Value visible for the user", position = 2, required = true)
	String display;
	@ApiModelProperty(notes = "Color", position = 3, required = true)
	String color;

	public EndpointStatusResponse(String value, String display, String color) {
		this.value = value;
		this.display = display;
		this.color = color;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getColor() {
		return color;
	}
}
