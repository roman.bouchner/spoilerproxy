package com.spoilerproxy.api.response;

public class ConnectionGraphResponse {
	String[][] items;

	public ConnectionGraphResponse(String[][] items) {
		this.items = items;
	}

	public String[][] getItems() {
		return items;
	}

	public void setItems(String[][] items) {
		this.items = items;
	}
}
