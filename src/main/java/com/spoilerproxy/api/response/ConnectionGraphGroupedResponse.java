package com.spoilerproxy.api.response;

import java.util.List;

public class ConnectionGraphGroupedResponse {
	List<GraphGrouped> items;

	public ConnectionGraphGroupedResponse(List<GraphGrouped> items) {
		this.items = items;
	}

	public List<GraphGrouped> getItems() {
		return items;
	}

	public void setItems(List<GraphGrouped> items) {
		this.items = items;
	}
}
