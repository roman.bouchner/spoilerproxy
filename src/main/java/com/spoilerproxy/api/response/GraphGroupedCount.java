package com.spoilerproxy.api.response;

public class GraphGroupedCount {
	String status;
	int count;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public GraphGroupedCount(String status, int count) {
		this.status = status;
		this.count = count;
	}
}
