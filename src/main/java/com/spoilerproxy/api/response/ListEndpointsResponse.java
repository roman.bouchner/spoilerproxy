package com.spoilerproxy.api.response;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class ListEndpointsResponse {
	@ApiModelProperty(notes = "List of endpoints", position = 1, required = true)
	List<EndpointResponse> endpointList;

	@ApiModelProperty(notes = "List of valid statuses", position = 2, required = true)
	List<EndpointStatusResponse> endpointStatuses;

	public List<EndpointResponse> getEndpointList() {
		return endpointList;
	}

	public void setEndpointList(List<EndpointResponse> endpointList) {
		this.endpointList = endpointList;
	}

	public List<EndpointStatusResponse> getEndpointStatuses() {
		return endpointStatuses;
	}

	public void setEndpointStatuses(List<EndpointStatusResponse> endpointStatuses) {
		this.endpointStatuses = endpointStatuses;
	}

}
