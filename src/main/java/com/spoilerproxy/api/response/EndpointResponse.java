package com.spoilerproxy.api.response;

import io.swagger.annotations.ApiModelProperty;

public class EndpointResponse {
	@ApiModelProperty(notes = "Unique ID of endpoint", position = 1, required = true)
	String id;
	@ApiModelProperty(notes = "Name of endpoint. Usually contains DNS name", position = 2, required = true,example = "www.service.com:443")
	String name;
	@ApiModelProperty(notes = "Spoiler status", position = 3, required = true, example = "on")
	String actualStatus;
	@ApiModelProperty(notes = "Actual live connections", position = 4, required = true, example = "1")
	long connections;

	public EndpointResponse(String id, String name, String actualStatus, long connections) {
		this.id = id;
		this.name = name;
		this.actualStatus = actualStatus;
		this.connections = connections;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getActualStatus() {
		return actualStatus;
	}

	public void setActualStatus(String actualStatus) {
		this.actualStatus = actualStatus;
	}

	public long getConnections() {
		return connections;
	}

	public void setConnections(long connections) {
		this.connections = connections;
	}
}
