package com.spoilerproxy.api.response;

import io.swagger.annotations.ApiModelProperty;

public class GetStatusResponse {
	@ApiModelProperty(notes = "Info if SOCKS proxy is running", position = 3, required = true, example = "true")
	boolean socksProxyRunning;

	@ApiModelProperty(notes = "Info about SOCKS proxy port", position = 4, required = true, example = "1080")
	int socksPort;

	@ApiModelProperty(notes = "Info about Spoiler Proxy version", position = 5, required = true, example = "1.0")
	String version;

	public boolean isSocksProxyRunning() {
		return socksProxyRunning;
	}

	public void setSocksProxyRunning(boolean socksProxyRunning) {
		this.socksProxyRunning = socksProxyRunning;
	}

	public int getSocksPort() {
		return socksPort;
	}

	public void setSocksPort(int socksPort) {
		this.socksPort = socksPort;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
