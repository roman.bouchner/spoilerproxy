package com.spoilerproxy.api.response;

import java.util.List;

public class GraphGrouped {
	int time;
	List<GraphGroupedCount> grouped;

	public GraphGrouped(int time, List<GraphGroupedCount> grouped) {
		this.time = time;
		this.grouped = grouped;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public List<GraphGroupedCount> getGrouped() {
		return grouped;
	}

	public void setGrouped(List<GraphGroupedCount> grouped) {
		this.grouped = grouped;
	}
}
