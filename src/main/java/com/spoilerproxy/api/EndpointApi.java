package com.spoilerproxy.api;

import com.spoilerproxy.api.handler.ParameterException;
import com.spoilerproxy.api.request.ChangeStatusByAddress;
import com.spoilerproxy.api.request.ChangeStatusByIdRequest;
import com.spoilerproxy.api.request.ClearEndpointRequest;
import com.spoilerproxy.api.response.ConnectionGraphGroupedResponse;
import com.spoilerproxy.api.response.ConnectionGraphResponse;
import com.spoilerproxy.api.response.EndpointResponse;
import com.spoilerproxy.api.response.EndpointStatusResponse;
import com.spoilerproxy.api.response.GetStatusResponse;
import com.spoilerproxy.api.response.GraphGrouped;
import com.spoilerproxy.api.response.GraphGroupedCount;
import com.spoilerproxy.api.response.ListEndpointsResponse;
import com.spoilerproxy.config.ProxyConfig;
import com.spoilerproxy.endpoint.EndpointService;
import com.spoilerproxy.endpoint.EndpointStatus;
import com.spoilerproxy.graph.ConnectionGraph;
import com.spoilerproxy.graph.GraphItem;
import com.spoilerproxy.socks.ConnectionId;
import com.spoilerproxy.socks.SocksServer;
import com.spoilerproxy.util.Utils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value="/api")
public class EndpointApi {

    @Autowired
    SocksServer socksServer;

    @Autowired
    EndpointService endpointService;

    @Autowired
    ProxyConfig config;

    @Autowired
    ConnectionGraph connectionGraph;

    @ApiOperation(value = "Returns endpoints and other proxy info. Warning: This method can be changed in future.")
    @RequestMapping(value="/list-endpoints",method = RequestMethod.GET)
    public @ResponseBody
    ListEndpointsResponse listEndpoints() {
        ListEndpointsResponse response = new ListEndpointsResponse();

        List<EndpointStatusResponse> endpointStatusResponseList = new ArrayList<>();
        for(EndpointStatus endpointStatus:EndpointStatus.values()) {
            endpointStatusResponseList.add(new EndpointStatusResponse(endpointStatus.toApiValue(),endpointStatus.name(),endpointStatus.getColor()));
        }
        response.setEndpointStatuses(endpointStatusResponseList);

        List<EndpointResponse> endpointResponses = endpointService.getEndpoins().stream().map(endpointInfo -> new EndpointResponse(endpointInfo.getId(),endpointInfo.getName(),endpointInfo.getStatus().toApiValue(),endpointInfo.getActive())).collect(Collectors.toList());
        response.setEndpointList(endpointResponses);

        return response;
    }

    @ApiOperation(value = "Returns status of spoiler proxy")
    @RequestMapping(value="/get-status",method = RequestMethod.GET)
    public @ResponseBody
    GetStatusResponse getStatus() {
        GetStatusResponse response = new GetStatusResponse();
        response.setSocksPort(config.getSocksPort());
        response.setSocksProxyRunning(socksServer.isRunning());
        response.setVersion(Utils.getVersion());

        return response;
    }

    @ApiOperation(value = "Sets spoiler status to given connectionId.")
    @RequestMapping(value="/change-status-by-id",method = RequestMethod.POST)
    public void changeStatusById(@RequestBody ChangeStatusByIdRequest request) throws ParameterException {
        ConnectionId connectionId = ConnectionId.fromId(request.getConnectionId());
        if(connectionId == null) {
            throw new ParameterException("Invalid connectionId");
        }
        EndpointStatus endpointStatus = EndpointStatus.fromApiValue(request.getState());
        if(endpointStatus == null) {
            throw new ParameterException("Invalid state");
        }
        boolean set = endpointService.setStatus(connectionId,endpointStatus);
        if(!set) {
            throw new ParameterException("ConnectionId does not exist");
        }
    }

    @ApiOperation(value = "Sets spoiler status to given address name.")
    @RequestMapping(value="/change-status-by-address",method = RequestMethod.POST)
    public void changeStatusByAddress(@RequestBody ChangeStatusByAddress request) throws ParameterException {
        EndpointStatus endpointStatus = EndpointStatus.fromApiValue(request.getState());
        if(endpointStatus == null) {
            throw new ParameterException("Invalid state");
        }
        boolean set = endpointService.setStatus(request.getAddress(),endpointStatus);
        if(!set) {
            throw new ParameterException("Address does not exist");
        }
    }

    @ApiOperation(value = "Removes all endpoints from table")
    @RequestMapping(value="/clear-endpoints",method = RequestMethod.POST)
    public void clearEndpoints() {
        endpointService.clear();
    }

    @ApiOperation(value = "Removes specific from table")
    @RequestMapping(value="/clear-endpoint",method = RequestMethod.POST)
    public void clearEndpoint(@RequestBody ClearEndpointRequest request) throws ParameterException {
        ConnectionId connectionId = ConnectionId.fromId(request.getConnectionId());
        if(connectionId == null) {
            throw new ParameterException("Invalid connectionId");
        }
        endpointService.remove(connectionId);
    }

    @RequestMapping(value="/get-connection-graph",method = RequestMethod.GET)
    public ConnectionGraphResponse getConnectionGraph() {
        List<GraphItem> list = connectionGraph.getItems();
        String[][] result = new String[list.size()][];
        for(int i=0; i<result.length; i++) {
            result[i] = new String[]{Integer.toString(list.get(i).getTime()),Integer.toString(list.get(i).getConnections())};
        }
        return new ConnectionGraphResponse(result);
    }

    @RequestMapping(value="/get-connection-graph-grouped",method = RequestMethod.GET)
    public ConnectionGraphGroupedResponse getConnectionGraphGrouped() {
        List<GraphItem> list = connectionGraph.getItems();
        List<GraphGrouped> result = list.stream().map(graphItem -> {
            List<GraphGroupedCount> ggc = new ArrayList<>();
            for(String status :graphItem.getGrouped().keySet()) {
                ggc.add(new GraphGroupedCount(status,graphItem.getGrouped().get(status)));
            }
            return new GraphGrouped(graphItem.getTime(),ggc);
        }).collect(Collectors.toList());
        return new ConnectionGraphGroupedResponse(result);
    }
}
