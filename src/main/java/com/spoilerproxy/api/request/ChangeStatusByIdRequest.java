package com.spoilerproxy.api.request;

import io.swagger.annotations.ApiModelProperty;

public class ChangeStatusByIdRequest {
	@ApiModelProperty(notes = "Connection Id", position = 1, example = ":185.183.8.62:443", required = true)
	String connectionId;
	@ApiModelProperty(notes = "Spoiler state. Values: 'on','off','slow','noReply'", position = 2, example = "off", required = true)
	String state;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}
}
