package com.spoilerproxy.api.request;

import io.swagger.annotations.ApiModelProperty;

public class ChangeStatusByAddress {

	@ApiModelProperty(notes = "Endpoint address", position = 1, example = "www.spoilerproxy.com:443", required = true)
	String address;
	@ApiModelProperty(notes = "Spoiler state. Values: 'on','off','slow','noReply'", position = 2, example = "off", required = true)
	String state;


	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
