package com.spoilerproxy.api.request;

import io.swagger.annotations.ApiModelProperty;

public class ClearEndpointRequest {
	@ApiModelProperty(notes = "Connection Id", position = 1, example = ":185.183.8.62:443", required = true)
	String connectionId;

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}
}
