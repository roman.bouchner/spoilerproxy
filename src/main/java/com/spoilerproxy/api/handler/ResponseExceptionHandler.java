package com.spoilerproxy.api.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Global place for converting exceptions to error codes
 */
@ControllerAdvice
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {
	/**
	 * Processing general errors. ErrorId is generated and sent to the client. ErrorId goes also into log file.
	 */
	private ResponseEntity<ErrorDetails> handleUnexpectedException(Throwable ex) {
		//hide error message to the client
		String errorMessage = "Unexpected exception.";

		ErrorDetails errorDetails = new ErrorDetails(errorMessage);
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);//500 -unexpected
	}

	@ExceptionHandler(Throwable.class)
	public final ResponseEntity<ErrorDetails> handleGeneralException(Throwable ex) {
		if(ex instanceof ParameterException) {
			return new ResponseEntity<>(new ErrorDetails(ex.getMessage()), HttpStatus.BAD_REQUEST); //400 - invalid parameters
		}
		return handleUnexpectedException(ex);
	}
}
