package com.spoilerproxy.socks.proxy;

import com.spoilerproxy.endpoint.EndpointService;
import com.spoilerproxy.endpoint.EndpointStatus;
import com.spoilerproxy.socks.ConnectionId;
import com.spoilerproxy.socks.protocol.ConnectResult;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Proxy {

	private InputStream clientIs;
	private OutputStream clientOs;

	private Socket serverSocket;
	private boolean storeSentData;
	private EndpointService endpointService;
	private ConnectionId connectionId;

	private ByteArrayOutputStream baos;

	public Proxy(InputStream is, OutputStream os, boolean storeSentData, EndpointService endpointService, ConnectionId connectionId) {
		this.clientIs = is;
		this.clientOs = os;
		this.storeSentData = storeSentData;
		this.endpointService = endpointService;
		this.connectionId = connectionId;
		if(storeSentData) {
			this.baos = new ByteArrayOutputStream();
		}
	}

	public ConnectResult connect(InetAddress ipAddress, int port) {
		try {
			serverSocket = new Socket(ipAddress.getHostAddress(),port);
			serverSocket.setSoTimeout(10); //small timeout to solve blocking calls
		} catch(UnknownHostException e) {
			return ConnectResult.HOST_UNREACHABLE;
		} catch (IllegalArgumentException e) {
			return ConnectResult.INVALID_PORT;
		}catch (IOException e) {
			return ConnectResult.CONNECTION_REFUSED;
		}
		return ConnectResult.SUCCESS;
	}

	public InetAddress getInetAddress() {
		return serverSocket.getLocalAddress();
	}

	public int getPort() {
		return serverSocket.getLocalPort();
	}

	public void doProxyWork() throws IOException {
		InputStream serverIs = serverSocket.getInputStream();
		OutputStream serverOs = serverSocket.getOutputStream();

		final int standardBuffSize = 4096;
		boolean running = true;
		int buffLen = standardBuffSize;
		byte[] buff = new byte[buffLen];

		while (running) {
			EndpointStatus endpointStatus = endpointService.getStatus(connectionId);
			if(endpointStatus == EndpointStatus.OFF) {
				//finish communication
				return;
			}
			if(endpointStatus == EndpointStatus.SLOW) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					return;
				}
				buffLen = 1; //each byte 10ms
			} else {
				buffLen = standardBuffSize;
			}

			//change buffer size if necessary
			if(buff.length != buffLen) {
				buff = new byte[buffLen];
			}

			//data from client
			try {
				int len = clientIs.read(buff);
				if (len < 0) {
					//client finish conversation
					running = false;
				} else {
					//forward it to the server
					serverOs.write(buff, 0, len);
					serverOs.flush();
					if(storeSentData) {
						baos.write(buff,0,len);
						if(baos.size() > 10000) {
							//prevent memory leak, do not store it anymore
							storeSentData = false;
							baos = null;
						}
					}
				}
			} catch (InterruptedIOException e) {
				//client does not have anything to send, so continue, maybe server has
			}

			if(endpointStatus != EndpointStatus.NO_REPLY) {
				//data from server
				try {
					int len = serverIs.read(buff);
					if(endpointStatus == EndpointStatus.BAD_REPLY) {
						//finish without the reply
						running = false;
					} else {
						if (len < 0) {
							//server finish conversation
							running = false;
						} else {
							//forward it to the client
							clientOs.write(buff, 0, len);
							clientOs.flush();
						}
					}
				} catch (InterruptedIOException e) {
					//server does not have anything to send, so continue, maybe client has
				}
			}
		}
	}

	public void close() throws IOException {
		try {
			if(clientIs != null) {
				clientIs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if(clientOs != null) {
				clientOs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if(serverSocket != null) {
				serverSocket.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public byte[] getSentData() {
		return baos.toByteArray();
	}

	private String print(byte[] arr, int len) {
		StringBuilder str = new StringBuilder();
		for(int i=0; i<len; i++) {
			if(arr[i] > 25 && arr[i] < 126) {
				byte b = arr[i];
				int bi = b & (0xff);
				str.append((char)bi);
			} else {
				str.append(" ");
			}
		}
		str.append("\n");
		for(int i=0; i<len; i++) {
			byte b = arr[i];
			int bi = b & (0xff);
			str.append(bi);str.append(",");

		}
		return str.toString();
	}
}
