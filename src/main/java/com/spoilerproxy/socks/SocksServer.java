package com.spoilerproxy.socks;

import com.spoilerproxy.config.ProxyConfig;
import com.spoilerproxy.dns.DnsStorageService;
import com.spoilerproxy.endpoint.EndpointService;
import com.spoilerproxy.endpoint.EndpointStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class SocksServer {

	@Autowired
	private DnsStorageService dnsStorageService;

	@Autowired
	private EndpointService endpointService;

	@Autowired
	private ProxyConfig config;

	private Logger logger = Logger.getLogger(SocksServer.class.getName());

	private Thread serverThread;
	private ServerSocket listenSocket;
	private volatile boolean running= false;
	private CopyOnWriteArrayList<Connection> activeConnections = new CopyOnWriteArrayList<>();

	public void start() {
		running= true;
		serverThread = new Thread(serverListener);
		serverThread.setName("ServerThread");
		serverThread.start();
	}

	public void stop() {
		//stop main server
		running = false;
		//stop also active connections
		for(Connection conn: activeConnections) {
			conn.close();
		}
	}

	public boolean isRunning() {
		return running;
	}

	protected void removeConnection(Connection proxyReceiver) {
		if(activeConnections.remove(proxyReceiver)) {
			logger.info("Connection removed");
		}
	}

	private Runnable serverListener = () -> {
		//open SOCKS listening port
		try {
			listenSocket = new ServerSocket( config.getSocksPort() );
			listenSocket.setSoTimeout(1000);
			logger.info("SOCKS5 server started and listening on port " + config.getSocksPort());
		} catch (IOException e) {
			logger.log(Level.SEVERE,"Cannot bind port " + config.getSocksPort());
			running = false;
		}

		while(running) {
			try {
				Socket socket = listenSocket.accept();
				logger.info("Connection from " + socket.getInetAddress().toString());
				if(activeConnections.size() > config.getMaxConnections()) {
					logger.severe("Too many connections, closing new connection. Maximum: " + config.getMaxConnections());
					socket.close();
				} else {
					//ok, create a new connection
					Connection connection = new Connection(this, endpointService, dnsStorageService,socket);
					activeConnections.add(connection);
					connection.start();
				}
			} catch (InterruptedIOException e) {
				//periodical timeout, do not log it
			} catch (IOException e) {
				//some error, log it
				logger.log(Level.SEVERE,"Cannot accept connection",e);
			}
		}

		logger.info("SOCKS5 server is down now");
	};

	public List<Connection> getConnections() {
		return Collections.unmodifiableList(activeConnections);
	}

	public long getActiveConnectionsNum(ConnectionId connectionId) {
		return activeConnections.stream().filter(connection -> connectionId.equals(connection.getConnectionId())).count();
	}

	/**
	 * Returns number of connections grouped by endpoint status
	 * @return
	 */
	public HashMap<String,Integer> getActiveAllConnectionsNumGrouped() {
		HashMap<String,Integer> result = getEmptyGrouped();

		for(Connection connection:activeConnections) {
			ConnectionId connectionId = connection.getConnectionId();
			//only if initialized
			if(connectionId != null) {
				EndpointStatus endpointStatus = endpointService.getStatus(connectionId);
				result.put(endpointStatus.toApiValue(), result.get(endpointStatus.toApiValue()) + 1);
			}
		}
		return result;
	}

	public HashMap<String,Integer> getEmptyGrouped() {
		HashMap<String,Integer> result = new HashMap<>();
		for(EndpointStatus endpointStatus:EndpointStatus.values()) {
			if(endpointStatus == EndpointStatus.OFF) {
				//does not make sense
				continue;
			}
			result.put(endpointStatus.toApiValue(),0);
		}
		return result;
	}
}
