package com.spoilerproxy.socks.protocol.enums;

public enum AType {
	IP4,
	IP6,
	DOMAIN
}

