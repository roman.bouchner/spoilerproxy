package com.spoilerproxy.socks.protocol.enums;

public enum CommandEnum {
	CONNECT,
	BIND,
	UDP
}
