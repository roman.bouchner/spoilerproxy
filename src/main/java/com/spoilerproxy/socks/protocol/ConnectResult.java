package com.spoilerproxy.socks.protocol;

public enum ConnectResult {
	SUCCESS,
	HOST_UNREACHABLE,
	CONNECTION_REFUSED,
	INVALID_PORT
}
