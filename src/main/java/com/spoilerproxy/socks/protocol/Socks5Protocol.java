package com.spoilerproxy.socks.protocol;

import com.spoilerproxy.socks.SocksException;
import com.spoilerproxy.socks.protocol.enums.AType;
import com.spoilerproxy.socks.protocol.enums.CommandEnum;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;

/**
 * Protocol implementation
 * https://www.ietf.org/rfc/rfc1928.txt
 */
public class Socks5Protocol {

	private final int SOCKS5_VERSION=0x05;

	private final int NO_AUTH_REQUIRED = 0x00;
	private final int NO_ACCEPTABLE_METHODS = 0xff;

	private final int COMMAND_CONNECT = 0x01;
	private final int COMMAND_BIND = 0x02;
	private final int COMMAND_UDP = 0x03;

	private final int ATYP_V4 = 0x01;
	private final int ATYP_DOMAINNAME= 0x03;
	private final int ATYP_V6 = 0x04;

	private InputStream is;
	private OutputStream os;

	public Socks5Protocol(InputStream is, OutputStream os) {
		this.is = is;
		this.os = os;
	}

	private int readByte() throws IOException {
		while(true) {
			try {
				int b = is.read();
				if (b == -1) {
					throw new IOException("End of stream");
				}
				return b;
			} catch(java.net.SocketTimeoutException e) {
				//try it again
				continue;
			}
		}
	}

	private void sendByte(int b) throws IOException {
		os.write(b);
	}

	private void processHeader() throws IOException, SocksException {
		int header = readByte();
		if(header != SOCKS5_VERSION) {
			throw new SocksException("Missing SOCKS5 header");
		}
	}

	public void processAuth() throws IOException, SocksException {
		processHeader();
		int numberOfMethods = readByte();

		boolean noAuth = false;
		for(int i=0; i<numberOfMethods; i++) {
			if(readByte() == NO_AUTH_REQUIRED) {
				//client accepts noauth
				noAuth = true;
			}
		}

		if(noAuth) {
			//clients wants NO_AUTH_REQUIRED
			//confirm it back to the client
			sendByte(SOCKS5_VERSION);
			sendByte(NO_AUTH_REQUIRED);
			os.flush();
		} else {
			//we do not support other authorization, send the info back
			sendByte(SOCKS5_VERSION);
			sendByte(NO_ACCEPTABLE_METHODS);
			os.flush();
			throw new SocksException("Unsupported auth");
		}
	}

	public CommandEnum processCommand() throws IOException, SocksException {
		processHeader();
		int cmd = readByte();
		switch (cmd) {
			case COMMAND_CONNECT: return CommandEnum.CONNECT;
			case COMMAND_BIND: return CommandEnum.BIND;
			case COMMAND_UDP: return CommandEnum.UDP;
			default: throw new SocksException("Unknown command");
		}
	}

	public AType processAType() throws IOException, SocksException {
		int reserved = readByte();
		if(reserved != 0) {
			throw new SocksException("Invalid reserved byte");
		}
		int atype = readByte();
		switch (atype) {
			case ATYP_V4: return AType.IP4;
			case ATYP_V6: return AType.IP6;
			case ATYP_DOMAINNAME: return AType.DOMAIN;
			default: throw new SocksException("Unknown atype");
		}
	}

	public InetAddress processIp4Address() throws IOException {
		return Inet4Address.getByAddress(processBytes(4));
	}

	public InetAddress processIp6Address() throws IOException {
		return Inet6Address.getByAddress(processBytes(16));
	}

	private byte[] processBytes(int len) throws IOException {
		byte[] bytes = new byte[len];
		for(int i=0; i<len; i++) {
			bytes[i] = (byte)readByte();
		}
		return bytes;
	}

	public String processDomainName() throws IOException {
		int len = readByte();
		StringBuilder result = new StringBuilder();
		for(int i=0; i<len; i++) {
			char ch = (char)readByte();
			result.append(ch);
		}
		return result.toString();
	}

	public int processPort() throws IOException {
		int b1 = readByte();
		int b2 = readByte();
		return b1<<8|b2;
	}

	public void processReply(ConnectResult result, InetAddress address, int port) throws IOException {
		sendByte(SOCKS5_VERSION);

		//result
		switch (result) {
			case SUCCESS: sendByte(0x00);break;
			case CONNECTION_REFUSED: sendByte(0x05);break;
			case HOST_UNREACHABLE: sendByte(0x04);break;
			case INVALID_PORT: sendByte(0x01);break;
			default:
				throw new RuntimeException();
		}

		//address
		sendByte(0x00); //reserved
		if(address instanceof Inet6Address) {
			sendByte(0x04); //IPV6
		} else if(address instanceof Inet4Address) {
			sendByte(0x01); //IPV4
		} else {
			throw new RuntimeException("Invalid address??");
		}
		byte[] adressBytes = address.getAddress();
		for(int i=0; i<adressBytes.length; i++) {
			sendByte(adressBytes[i]);
		}

		//port
		sendByte((port & 0xFF00)>>8);
		sendByte(port & 0xFF);
		os.flush();
	}

	private int byteToInt(byte val) {
		return val & (0xff);
	}

}
