package com.spoilerproxy.socks;

import com.spoilerproxy.dns.DnsStorageService;
import com.spoilerproxy.dns.util.SillyDNSParser;
import com.spoilerproxy.endpoint.EndpointService;
import com.spoilerproxy.socks.protocol.Socks5Protocol;
import com.spoilerproxy.socks.protocol.enums.AType;
import com.spoilerproxy.socks.protocol.enums.CommandEnum;
import com.spoilerproxy.socks.proxy.Proxy;
import com.spoilerproxy.socks.protocol.ConnectResult;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.Socket;
import java.util.logging.Logger;

public class Connection {
	Logger logger = Logger.getLogger(Connection.class.getName());

	private Thread thread;
	private SocksServer proxyServer;
	private DnsStorageService dnsStorageService;
	EndpointService endpointService;
	private Socket socket;
	private InputStream is;
	private OutputStream os;
	private volatile ConnectionId connectionId;

	public Connection(SocksServer proxyServer, EndpointService endpointService, DnsStorageService dnsStorageService, Socket socket) {
		this.proxyServer = proxyServer;
		this.endpointService = endpointService;
		this.dnsStorageService = dnsStorageService;
		this.socket = socket;
	}

	public void start() {
		thread = new Thread(connection);
		thread.start();
	}

	public void close() {
		proxyServer.removeConnection(this);
		try {
			if(is != null) {
				is.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if(os != null) {
				os.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ConnectionId getConnectionId() {
		return connectionId;
	}

	private Runnable connection = ()->{
		try {
			try {
				//small timeout to solve blocking call
				socket.setSoTimeout(10);

				is = socket.getInputStream();
				os = socket.getOutputStream();

				Socks5Protocol socks5 = new Socks5Protocol(is,os);

				//process authorization initial sequence
				socks5.processAuth();

				//process command
				CommandEnum command = socks5.processCommand();
				AType type = socks5.processAType();

				//obtain destination address and port
				InetAddress ipAddress = null;
				String domain = null;
				switch (type) {
					case IP4: ipAddress = socks5.processIp4Address();break;
					case IP6: ipAddress = socks5.processIp6Address();break;
					case DOMAIN:
						domain = socks5.processDomainName();
						ipAddress = InetAddress.getByName(domain);
					break;
				}
				int port = socks5.processPort();

				connectionId = new ConnectionId(domain,ipAddress.getHostAddress(),port);

				//register connections as endpoint
				endpointService.addConnection(connectionId,true);

				logger.info("Success request, " + command.name() + " " + type + " IP:" + (ipAddress!=null?ipAddress.toString():"N/A") + " Domain:" + (domain!=null?domain:"N/A") + " Port:" + port);

				if(command == CommandEnum.CONNECT) {
					//try to connect to the destination
					boolean isDNSQuery = port == 53;
					Proxy proxy = new Proxy(is, os, isDNSQuery, endpointService, connectionId);
					ConnectResult result = proxy.connect(ipAddress, port);
					try {
						if (result == ConnectResult.SUCCESS) {
							//ok, report it to the client
							socks5.processReply(result, proxy.getInetAddress(), proxy.getPort());
							logger.info("Success connection, lets do proxy " + proxy.getInetAddress() + " " + proxy.getPort());
							proxy.doProxyWork();
						} else {
							//some error, report it to the client
							socks5.processReply(result, type==AType.IP4?Inet4Address.getByName("0.0.0.0"): Inet6Address.getByName("::1"), 0);
							logger.info("Error connection to the destination " + result.name());
						}
					} finally {
						proxy.close();
					}

					//extract DNS query
					if(isDNSQuery) {
						SillyDNSParser dnsParser = new SillyDNSParser(proxy.getSentData());
						String dnsDomain = dnsParser.parseDomain();
						if(dnsDomain !=null) {
							dnsStorageService.addDomain(dnsDomain,true);
							System.out.println(dnsDomain);
						}
					}
				}

				if(command == CommandEnum.UDP) {
					logger.warning("UDP command is not implemented yet");
				}

				if(command == CommandEnum.BIND) {
					logger.warning("BIND command is not implemented yet");
				}

			} catch (IOException e) {
				e.printStackTrace();
			} catch (SocksException e) {
				//invalid protocol
				logger.info("Unknown protocol: " + e.getMessage());
			}
		} finally {
			close();
		}
	};

}
