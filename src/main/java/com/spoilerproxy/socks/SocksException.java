package com.spoilerproxy.socks;

public class SocksException extends Exception {
	public SocksException(String message) {
		super(message);
	}
}
