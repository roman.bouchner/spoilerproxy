package com.spoilerproxy.socks;

import java.util.Objects;

/**
 * Info about destination connection. Either host or ipAddress is filled.
 */
public class ConnectionId {
	private final String host;
	private final String ipAddress;
	private final int port;
	private static final String sep = ":";

	public ConnectionId(String host, String ipAddress, int port) {
		this.host = host;
		this.ipAddress = ipAddress;
		this.port = port;
	}

	public String getHost() {
		return host;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public int getPort() {
		return port;
	}

	public String toId() {
		StringBuilder str = new StringBuilder();
		if(host != null) {
			str.append(host);
		}
		str.append(sep);
		if(ipAddress!=null) {
			str.append(ipAddress);
		}
		str.append(sep);
		str.append(port);
		return str.toString();
	}

	/**
	 * Converts string to connectionId object.
	 * @param str returns null if convert is not possible
	 * @return
	 */
	public static ConnectionId fromId(String str) {
		if(str == null) {
			return null;
		}
		String[] vals = str.split(sep);
		if(vals.length != 3) {
			return null;
		}
		String host = vals[0].isEmpty()?null:vals[0];
		String ipAddress = vals[1].isEmpty()?null:vals[1];
		try {
			int port = Integer.parseInt(vals[2]);
			return new ConnectionId(host,ipAddress,port);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	@Override
	public String toString() {
		return (getHost()!=null?getHost():getIpAddress()) + ":" + port;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ConnectionId that = (ConnectionId) o;
		return port == that.port &&
				Objects.equals(host, that.host) &&
				Objects.equals(ipAddress, that.ipAddress);
	}

	@Override
	public int hashCode() {
		return Objects.hash(host, ipAddress, port);
	}
}
