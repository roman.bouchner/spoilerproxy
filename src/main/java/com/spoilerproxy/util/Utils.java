package com.spoilerproxy.util;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Utils {
	public static String getVersion() {
		InputStream is = Utils.class.getClassLoader().getResourceAsStream("version.txt");
		if (is == null) {
			return "N/A";
		} else {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				IOUtils.copy(is, baos);
				return baos.toString();
			} catch (IOException e) {
				return "N/A";
			}
		}
	}
}
