package com.spoilerproxy.endpoint;

public class EndpointInfo {
	private final String id;
	private final String name;
	private final EndpointStatus status;
	private final long active;

	public EndpointInfo(String id, String name, EndpointStatus status, long active) {
		this.id = id;
		this.name = name;
		this.status = status;
		this.active = active;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public EndpointStatus getStatus() {
		return status;
	}
	
	public long getActive() {
		return active;
	}
}
