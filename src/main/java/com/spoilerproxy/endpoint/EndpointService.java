package com.spoilerproxy.endpoint;

import com.spoilerproxy.dns.DnsStorageService;
import com.spoilerproxy.repository.RepositoryService;
import com.spoilerproxy.socks.ConnectionId;
import com.spoilerproxy.socks.SocksServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class EndpointService {
	@Autowired
	private DnsStorageService dnsStorageService;

	@Autowired
	private SocksServer socksServer;

	@Autowired
	private RepositoryService repositoryService;

	private ConcurrentHashMap<String,Endpoint> map = new ConcurrentHashMap<>();

	public void addConnection(ConnectionId connectionId,boolean save) {
		//add as a new endpoint if not present.
		if(map.putIfAbsent(connectionId.toId(),new Endpoint(connectionId,EndpointStatus.ON)) == null) {
			//new endpoint, ask for save
			if(save) {
				repositoryService.createSaveRequest();
			}
		}
	}

	public EndpointStatus getStatus(ConnectionId connectionId) {
		Endpoint endpoint = map.get(connectionId.toId());
		if(endpoint != null) {
			return endpoint.getStatus();
		} else {
			return EndpointStatus.ON;
		}
	}

	public boolean setStatus(ConnectionId connectionId, EndpointStatus status) {
		Endpoint endpoint = map.get(connectionId.toId());
		if(endpoint != null) {
			endpoint.setStatus(status);
			return true;
		}
		return false;
	}
	
	public boolean setStatus(String name, EndpointStatus status) {
		List<Endpoint> endpoints = map.values().stream().filter(endpoint -> connectionIdToName(endpoint.getId()).equals(name)).collect(Collectors.toList());
		for (Endpoint e:endpoints) {
			setStatus(e.getId(),status);
		}
		return endpoints.size() > 0;
	}

	private String connectionIdToName(ConnectionId connectionId) {
		//maybe proxy sent us hostname directly
		String name = connectionId.getHost();
		if(name == null) {
			//try our dns storage service
			name = dnsStorageService.getDomain(connectionId.getIpAddress());
		}
		if(name == null) {
			//use ip address instead
			name = connectionId.getIpAddress();
		}
		//include port
		return name + ":" + connectionId.getPort();
	}
	
	public List<EndpointInfo> getEndpoins() {
		return map.values().stream().map(endpoint -> {
			String id = endpoint.getId().toId();
			String name = connectionIdToName(endpoint.getId());
			
			long active = socksServer.getActiveConnectionsNum(endpoint.getId());

			return new EndpointInfo(id,name,endpoint.getStatus(),active);
		}).sorted(Comparator.comparing(EndpointInfo::getName)
		).collect(Collectors.toList());
	}

	public void clear() {
		map.forEach((key,value)->{
			long active = socksServer.getActiveConnectionsNum(value.getId());
			if(active == 0) {
				map.remove(key);
			}
		});
		repositoryService.createSaveRequest();
	}

	public void remove(ConnectionId connectionId) {
		Endpoint endpoint = map.get(connectionId.toId());
		map.forEach((key,value)->{
			if(value.getId().equals(connectionId)) {
				map.remove(key);
				return;
			}
		});
		repositoryService.createSaveRequest();
	}
}
