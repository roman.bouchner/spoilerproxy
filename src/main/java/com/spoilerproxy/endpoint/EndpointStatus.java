package com.spoilerproxy.endpoint;

public enum EndpointStatus {
	OFF("off","#FF5630"),
	NO_REPLY("noReply","#FFAB00"),
	BAD_REPLY("badReply","#CD6889"),
	SLOW("slow","#00B8D9"),
	ON("on","#36B37E");

	String apiValue;
	String color;

	EndpointStatus(String apiValue, String color) {
		this.apiValue = apiValue;
		this.color = color;
	}

	public static EndpointStatus fromApiValue(String val) {
		for(EndpointStatus endpointStatus:EndpointStatus.values()) {
			if(endpointStatus.toApiValue().equals(val)) {
				return endpointStatus;
			}
		}
		return null;
	}

	public String toApiValue() {
		return apiValue;
	}

	public String getColor() {
		return color;
	}
}
