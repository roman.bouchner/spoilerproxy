package com.spoilerproxy.endpoint;

import com.spoilerproxy.socks.ConnectionId;

public class Endpoint {
	private final ConnectionId id;
	private EndpointStatus status;

	public Endpoint(ConnectionId id, EndpointStatus status) {
		this.id = id;
		this.status = status;
	}

	public ConnectionId getId() {
		return id;
	}

	public synchronized EndpointStatus getStatus() {
		return status;
	}

	public synchronized void setStatus(EndpointStatus status) {
		this.status = status;
	}
}
