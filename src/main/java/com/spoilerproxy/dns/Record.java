package com.spoilerproxy.dns;

public class Record {
	private final String ip;
	private final String name;

	public Record(String ip, String name) {
		this.ip = ip;
		this.name = name;
	}

	public String getIp() {
		return ip;
	}

	public String getName() {
		return name;
	}
}
