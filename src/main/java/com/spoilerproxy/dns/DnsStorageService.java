package com.spoilerproxy.dns;

import com.spoilerproxy.repository.RepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class DnsStorageService {
	private static final int MAX_DOMAIN = 5000;
	private Logger logger = Logger.getLogger(DnsStorageService.class.getName());

	@Autowired
	private RepositoryService repositoryService;

	private ConcurrentHashMap<String,String> ipToName = new ConcurrentHashMap<>();

	public void addDomain(String name, boolean save) {
		try {
			InetAddress[] inetAddresses = InetAddress.getAllByName(name);
			for(InetAddress inetAddress:inetAddresses) {
				ipToName.put(inetAddress.getHostAddress(), name);
				//prevent memory leak
				if (ipToName.size() > MAX_DOMAIN) {
					logger.warning("Max domain number reached, cleaning");
					ipToName.clear();
				}
			}
			//save changes
			if(save) {
				repositoryService.createSaveRequest();
			}
		} catch (UnknownHostException e) {
			//ok, do not add anything
			logger.warning("Unknown host: " + name);
		}
	}

	public String getDomain(InetAddress inetAddress) {
		return ipToName.get(inetAddress.getHostAddress());
	}

	public String getDomain(String inetAddress) {
		return ipToName.get(inetAddress);
	}

	public List<Record> getAllRecords() {
		return ipToName.entrySet().stream().map((entry)->new Record(entry.getKey(),entry.getValue())).collect(Collectors.toList());
	}
}
