package com.spoilerproxy.dns.util;

/**
 * Really stupid parser for proof of concept, should be really improved.
 */
public class SillyDNSParser {
	byte[] data;
	int pos;

	private static class ParseError extends Exception {

	}

	public SillyDNSParser(byte[] data) {
		this.data = data;
		pos = 14;
	}

	/**
	 * Returns first parsed domain from DNS request.
	 * @return null if any error
	 */
	public String parseDomain() {
		StringBuilder result = new StringBuilder();
		try {
			while (true) {
				int size = readNextByte();
				if(size == 0) {
					break;
				}
				for(int i=0; i<size;i++) {
					result.append((char)readNextByte());
				}
				result.append(".");
			}
		} catch(ParseError e) {
			return null;
		}
		if(result.length() > 0) {
			result.deleteCharAt(result.length() - 1); //remove last dot
		}
		return result.toString();
	}

	public int readNextByte() throws ParseError {
		if(pos >= data.length) {
			throw new ParseError();
		}
		return (data[pos++] & 0xFF); //conversion byte to int (byte is signed)
	}
}
