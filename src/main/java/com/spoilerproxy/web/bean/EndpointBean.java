package com.spoilerproxy.web.bean;

import java.util.List;

public class EndpointBean {
	String id;
	String name;
	String actualStatus;
	List<StatusBean> statuses;
	long connections;

	public EndpointBean(String id, String name, String actualStatus, List<StatusBean> statuses, long connections) {
		this.id = id;
		this.name = name;
		this.actualStatus = actualStatus;
		this.statuses = statuses;
		this.connections = connections;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getActualStatus() {
		return actualStatus;
	}

	public void setActualStatus(String actualStatus) {
		this.actualStatus = actualStatus;
	}

	public List<StatusBean> getStatuses() {
		return statuses;
	}

	public void setStatuses(List<StatusBean> statuses) {
		this.statuses = statuses;
	}

	public long getConnections() {
		return connections;
	}

	public void setConnections(int connections) {
		this.connections = connections;
	}
}
