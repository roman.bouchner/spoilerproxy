package com.spoilerproxy.web.bean;

public class SocksServerBean {
	boolean running;
	int port;

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
}
