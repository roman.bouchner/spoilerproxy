package com.spoilerproxy.web.bean;

public class StatusBean {
	String value;
	String display;


	public StatusBean(String value, String display) {
		this.value = value;
		this.display = display;
	}

	public String getValue() {
		return value;
	}

	public String getDisplay() {
		return display;
	}
}
