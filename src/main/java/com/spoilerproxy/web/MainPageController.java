package com.spoilerproxy.web;

import com.spoilerproxy.config.ProxyConfig;
import com.spoilerproxy.endpoint.Endpoint;
import com.spoilerproxy.endpoint.EndpointService;
import com.spoilerproxy.endpoint.EndpointStatus;
import com.spoilerproxy.socks.SocksServer;
import com.spoilerproxy.util.Utils;
import com.spoilerproxy.web.bean.EndpointBean;
import com.spoilerproxy.web.bean.SocksServerBean;
import com.spoilerproxy.web.bean.StatusBean;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class MainPageController {

	@Autowired
	SocksServer socksServer;

	@Autowired
	EndpointService endpointService;

	@Autowired
	ProxyConfig config;

	@GetMapping({"/fallback"})
	public String mainPage(Model model) {
		SocksServerBean socksServerBean = new SocksServerBean();
		socksServerBean.setPort(config.getSocksPort());
		socksServerBean.setRunning(socksServer.isRunning());
		model.addAttribute("socksServer",socksServerBean);
		model.addAttribute("version", Utils.getVersion());

		List<StatusBean> statusBeanList = new ArrayList<>();
		for(EndpointStatus endpointStatus:EndpointStatus.values()) {
			statusBeanList.add(new StatusBean(endpointStatus.toApiValue(),endpointStatus.name()));
		}

		List<EndpointBean> endpointBeanList = endpointService.getEndpoins().stream().map(endpointInfo -> new EndpointBean(endpointInfo.getId(),endpointInfo.getName(),endpointInfo.getStatus().toApiValue(),statusBeanList,endpointInfo.getActive())).collect(Collectors.toList());;
		model.addAttribute("endpoints",endpointBeanList);

		return "main";
	}

}
